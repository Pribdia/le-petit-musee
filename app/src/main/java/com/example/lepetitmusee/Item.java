package com.example.lepetitmusee;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.os.Build.*;

public class Item implements Parcelable {

    private String id;
    private String name;
    private List<String> category = new ArrayList<String>();
    private String description;
    private List<Integer> timeFrame = new ArrayList<Integer>();

    private boolean isSection = false;
    private int year = 0;
    private String brand;
    private List<String> technicalDetails = new ArrayList<String>();
    private boolean workig;
    private List<Picture> pictures = new ArrayList<Picture>();
    private Bitmap thumbnail;
    private Parcel dest;
    private int flags;

    public Item() {

    }

    public boolean isSection() {
        if (isSection && this instanceof ItemSection) {
            return true;
        }
        return false;
    }

    public boolean isResearch(String researchString) {
        if (isInName(researchString) || isInCategory(researchString) || isInBrand(researchString) || isInDesc(researchString)) {
            return true;
        }
        return false;
    }

    public boolean isInName(String researchString) {
        if (name.toLowerCase().contains(researchString.toLowerCase())) {
            return true;
        }
        return false;
    }

    public boolean isInDesc(String researchString) {
        if (description != null && description.toLowerCase().contains(researchString.toLowerCase())) {
            return true;
        }
        return false;
    }

    public boolean isInBrand(String researchString) {
        if (brand != null && brand.toLowerCase().contains(researchString.toLowerCase())) {
            return true;
        }
        return false;
    }

    public void setSection(boolean section) {
        isSection = section;
    }

    public static final String TAG = Item.class.getSimpleName();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getNameAndYear() {
        String result = name;
        if (year > 0) {
            result = result + " ( "+ year + " ) ";
        }
        return result;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCategory() {
        return category;
    }

    public String getCategoryDisplay() {
        String categoryDisplay ="";

        for (String oneCategory : this.category) {
            oneCategory = oneCategory.substring(0, 1).toUpperCase() + oneCategory.substring(1);
            categoryDisplay = categoryDisplay + "( " + oneCategory + " ) ";
        }

        return categoryDisplay;
    }

    public void setCategory(List<String> category) {
        this.category = category;
    }

    public void addCategory(String category) { this.category.add(category); }

    public String getDescription() {
        return description;
    }



    public void setDescription(String description) {
        this.description = description;
    }

    public List<Integer> getTimeFrame() {
        return timeFrame;
    }

    public List<String> getStringTimeFrame() {
        List<String> stringTimeFrame = new ArrayList<String>();

        for (int time : timeFrame ) {
            stringTimeFrame.add(String.valueOf(time));
        }

        return stringTimeFrame;
    }

    public void setTimeFrame(List<Integer> timeFrame) {
        this.timeFrame = timeFrame;
    }

    public void addTimeFrame(Integer timeFrame) { this.timeFrame.add(timeFrame); }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<String> getTechnicalDetails() {
        return technicalDetails;
    }

    public void setTechnicalDetails(List<String> technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public void addTechnicalDetails(String technicalDetail) {
        this.technicalDetails.add(technicalDetail);
    }

    public boolean isWorkig() {
        return workig;
    }

    public String stringIsWorkig() {
        if (workig) {
            return "Fonctionnel";
        }
        return "Non fonctionnel";
    }

    public void setWorkig(boolean workig) {
        this.workig = workig;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }

    public void addPicture(Picture picture) { this.pictures.add(picture); }

    public void addPicture(String name, String description) {
        this.pictures.add(new Picture(name,description));
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isInCategory(String category) {
        for (int i = 0; i < getCategory().size(); i++) {
            if (getCategory().get(i).equals(category)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeList(this.category);
        dest.writeString(this.description);
        dest.writeList(this.timeFrame);
        dest.writeInt(this.workig ? 1 : 0);
        dest.writeInt(this.year);
        dest.writeString(this.brand);
        dest.writeList(this.technicalDetails);
        dest.writeList(this.pictures);

    }

    public static final Creator<Item> CREATOR = new Creator<Item>()
    {
        @Override
        public Item createFromParcel(Parcel source)
        {
            return new Item(source);
        }

        @Override
        public Item[] newArray(int size)
        {
            return new Item[size];
        }
    };

    public Item(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();

        this.category = new ArrayList<String>();
        in.readList(this.category,String.class.getClassLoader());

        this.description = in.readString();

        this.timeFrame = new ArrayList<Integer>();
        in.readList(this.timeFrame,Integer.class.getClassLoader());

        this.workig = in.readInt() == 1;

        this.year = in.readInt();
        this.brand = in.readString();

        this.technicalDetails = new ArrayList<String>();
        in.readList(this.technicalDetails,String.class.getClassLoader());

        this.pictures = new ArrayList<Picture>();
        in.readList(this.pictures,Picture.class.getClassLoader());
    }

}
