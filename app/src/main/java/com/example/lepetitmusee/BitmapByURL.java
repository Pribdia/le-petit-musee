package com.example.lepetitmusee;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.InputStream;
import java.net.URL;

public class BitmapByURL {

    URL imageURL;
    Bitmap imageBitmap = null;

    public BitmapByURL(URL imageURL) {
        this.imageURL = imageURL;
    }

    public Bitmap getImageBitmap() {

        if (this.imageBitmap == null) {
            try {
                if (imageURL != null) {
                    InputStream in = new java.net.URL(imageURL.toString()).openStream();
                    this.imageBitmap = BitmapFactory.decodeStream(in);
                }
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }
        return this.imageBitmap;
    }


}
