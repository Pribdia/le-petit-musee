package com.example.lepetitmusee;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class CatalogActivity extends AppCompatActivity {

    RecyclerView itemsRecyclerView;
    IconicAdapter adapter;
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // RecyclerView
        itemsRecyclerView = (RecyclerView) findViewById(R.id.itemsRecyclerView);
        itemsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        itemsRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        adapter = new IconicAdapter();
        itemsRecyclerView.setAdapter(adapter);

        final FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final OvershootInterpolator interpolator = new OvershootInterpolator();
                ViewCompat.animate(fab).
                        rotation(1000).
                        withLayer().
                        setDuration(10000).
                        setInterpolator(interpolator).
                        start();

                ItemList.UPDATE_CATEGORY_LIST();
                ItemList.GET_CATALOG(CatalogActivity.this);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.app_bar_search);
        searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.tri_alphabet) {
            ItemList.SORT_BY_ALPHABETICAL_ORDER();
            adapter = new IconicAdapter();
            itemsRecyclerView.setAdapter(adapter);
            adapter.getFilter().filter(searchView.getQuery());
            Toast.makeText(CatalogActivity.this, "La liste est triée par ordre alphabétique", Toast.LENGTH_SHORT).show();
            return true;
        } else if (id == R.id.tri_chrono) {
            ItemList.SORT_BY_CHRONOLOGICAL_ORDER_WITH_SECTION();
            adapter = new IconicAdapter();
            itemsRecyclerView.setAdapter(adapter);
            adapter.getFilter().filter(searchView.getQuery());
            Toast.makeText(CatalogActivity.this, "La liste est triée par ordre chronologique", Toast.LENGTH_SHORT).show();
            return true;
        } else if (id == R.id.tri_category) {
            ItemList.SORT_BY_CATEGORY_WITH_SECTION();
            adapter = new IconicAdapter();
            itemsRecyclerView.setAdapter(adapter);
            adapter.getFilter().filter(searchView.getQuery());
            Toast.makeText(CatalogActivity.this, "La liste est triée par categories", Toast.LENGTH_SHORT).show();
        }



        return super.onOptionsItemSelected(item);
    }

    // [DEBUT] : Gestion RecyclerView
    // Adapter (controler de ligne)
    class IconicAdapter extends RecyclerView.Adapter<RowHolder> implements Filterable {

        List<Item> itemList = new ArrayList<Item>();
        List<Item> itemListFilter;

        public IconicAdapter() {
            this.itemList.addAll(ItemList.GET_ITEM_LIST());
        }

        public Item getItemPosition(int position) {
            return itemList.get(position);
        }

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.row_item, parent, false)));
        }
        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            Item itemSelected = itemList.get(position);
            holder.bindModel(itemSelected);
        }
        @Override
        public int getItemCount() {
            return(itemList.size());
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();

                    if (charString.isEmpty()) {
                        itemListFilter = ItemList.GET_ITEM_LIST();
                    } else {
                        itemListFilter = ItemList.FILTER_LIST_BY_SEARCH(charString);
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = itemListFilter;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    itemList.clear();
                    itemList = (ArrayList<Item>) filterResults.values;

                    //itemsRecyclerView.setAdapter(adapter);
                    notifyDataSetChanged();
                }
            };
        }
    }

    // Gestionnaire d'affichage d'une ligne

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView thumbnailImageView;

        TextView nameTextView;
        TextView brandTextView ;
        TextView categoryTextView;
        ConstraintLayout itemRowView;


        @SuppressLint("WrongViewCast")
        RowHolder(View row) {
            super(row);

            thumbnailImageView = (ImageView) row.findViewById(R.id.thumbnailImageView);

            nameTextView = (TextView) row.findViewById(R.id.nameTextView);
            brandTextView =(TextView) row.findViewById(R.id.brandTextView);
            categoryTextView = (TextView) row.findViewById(R.id.categoryTextView);
            itemRowView = (ConstraintLayout) row.findViewById(R.id.itemRowView);
            itemRowView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v){
            int itemPosition = itemsRecyclerView.getChildLayoutPosition(v);

            Item itemSelected = adapter.getItemPosition(itemPosition);

            Intent itemView = new Intent(CatalogActivity.this, ItemActivity.class);
            itemView.putExtra(Item.TAG,itemSelected);
            startActivity(itemView);
        }
        void bindModel(Item itemSelected) {
            /*itemRowView.removeView(nameTextView);
            if (itemSelected.isSection()) {
                nameTextView.setTypeface(nameTextView.getTypeface(), Typeface.BOLD);
            }*/
            nameTextView.setText(itemSelected.getNameAndYear());
            brandTextView.setText(itemSelected.getBrand());
            categoryTextView.setText(itemSelected.getCategoryDisplay());

            SetThumbnailByUrlTask setImageBadgeByUrlTask = new SetThumbnailByUrlTask(itemSelected, thumbnailImageView);
            setImageBadgeByUrlTask.execute();

        }
    }

    // [FIN] : Gestion RecyclerView

}
