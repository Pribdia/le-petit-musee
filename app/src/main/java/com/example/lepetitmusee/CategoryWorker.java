package com.example.lepetitmusee;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class CategoryWorker extends AsyncTask<URL,Integer, Void> {

    @Override
    protected Void doInBackground(URL... urls) {

        HttpURLConnection connectionGetCategory;
        InputStream inputStreamGetCategory;
        JSONResponseHandlerCategory jsonResponseHandlerCategory = new JSONResponseHandlerCategory();

        try {
            URL apiUrlGetCategory = WebServiceUrl.buildAllCategory();
            connectionGetCategory = (HttpURLConnection) apiUrlGetCategory.openConnection();
            try {
                inputStreamGetCategory = new BufferedInputStream(connectionGetCategory.getInputStream());
                jsonResponseHandlerCategory.readJsonStream(inputStreamGetCategory);
            } finally {
                connectionGetCategory.disconnect();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        super.onPostExecute(v);
    }
}
