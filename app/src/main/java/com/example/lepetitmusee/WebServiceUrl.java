package com.example.lepetitmusee;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {

    // constant string used as s parameter for lookuptable
    private static final String CUR_YEAR = "1920";

    private static final String DOMAIN = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(DOMAIN)
                .appendPath(PATH_1);
        return builder;
    }

    // URL to all item (List id item)
    private static final String ALL_ITEM = "ids";

    public static URL buildAllItem() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ALL_ITEM);
        URL url = new URL(builder.build().toString());
        return url;
    }


    // URL form all category
    private static final String CATEGORY = "categories";

    public static URL buildAllCategory() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(CATEGORY);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // URL form catalog
    private static final String CATALOG = "catalog";

    public static URL buildCatalog() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // URL from item
    private static final String ITEMS = "items";

    public static URL buildItem(String idItem) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEMS)
                .appendPath(idItem);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // URL from thumbnail
    private static final String THUMBNAIL = "thumbnail";

    public static URL buildThumbnail(String idItem) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEMS)
                .appendPath(idItem)
                .appendPath(THUMBNAIL);

        URL url = new URL(builder.build().toString());
        return url;
    }

    // URL from image
    private static final String IMAGES = "images";

    public static URL buildPictures(String idItem,String imageId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ITEMS)
                .appendPath(idItem)
                .appendPath(IMAGES)
                .appendPath(imageId);

        URL url = new URL(builder.build().toString());
        return url;
    }

    // URL demo
    private static final String DEMOS = "demos";

    public static URL buildDemo() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(DEMOS);

        URL url = new URL(builder.build().toString());
        return url;
    }
}
