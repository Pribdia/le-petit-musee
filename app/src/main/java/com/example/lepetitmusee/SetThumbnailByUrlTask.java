package com.example.lepetitmusee;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class SetThumbnailByUrlTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;
    String itemId;
    public SetThumbnailByUrlTask(Item item, ImageView bmImage) {
        this.bmImage = bmImage;
        this.itemId = item.getId();
    }

    protected Bitmap doInBackground(String... urls) {
        Bitmap bmp = null;
        try {
            URL imageURL = WebServiceUrl.buildThumbnail(itemId);

            BitmapByURL bitmapByURL = new BitmapByURL(imageURL);
            bmp = bitmapByURL.getImageBitmap();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return bmp;
    }
    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);

    }
}
