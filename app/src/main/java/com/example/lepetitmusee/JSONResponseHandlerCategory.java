package com.example.lepetitmusee;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class JSONResponseHandlerCategory {
    private static final String TAG = JSONResponseHandlerCategory.class.getSimpleName();

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCategory(reader);
        } finally {
            reader.close();
        }
    }

    public void readCategory(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            ItemList.GET_CATEGORY().add(reader.nextString());
        }
        reader.endArray();
    }
}
