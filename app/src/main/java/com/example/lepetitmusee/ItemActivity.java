package com.example.lepetitmusee;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ItemActivity extends AppCompatActivity {

    Item item;
    LinearLayout detailView;
    LinearLayout pictureView;
    int detailStyle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        detailStyle = android.R.style.TextAppearance_Medium;

        item = (Item) getIntent().getParcelableExtra(Item.TAG);
        TextView itemNameTextView = (TextView) findViewById(R.id.itemName);

        detailView = (LinearLayout) findViewById(R.id.detailView);
        pictureView = (LinearLayout) findViewById(R.id.pictureView);

        itemNameTextView.setText(item.getName());

        // ajout des images qui ne fonctionne pas, erreur lors du téléchargement
        addPictures();
        addDetails();

    }

    private void addPictures() {
        AddPictureByUrlTask addPictureByUrlTask = new AddPictureByUrlTask(this,pictureView,item);
        addPictureByUrlTask.execute();
    }
    private void addDetails() {

        addDetail(item.getName(), "Nom");
        addDetail(item.getCategory(),"Catégories");
        addDetail(item.getDescription(),"Decription");
        addDetail(item.getStringTimeFrame(),"Date de commercialisation");

        addDetail(String.valueOf(item.getYear()),"Année de création");
        addDetail(item.getBrand(),"Marque");
        addDetail(item.getTechnicalDetails(),"Détails téchniques");
        addDetail(item.stringIsWorkig(), "État");

    }


    private void addTitleDetail(String title) {
        if (title != null) {
            TextView textTitle = new TextView(getApplicationContext());

            title = title + " : ";
            title = title.substring(0, 1).toUpperCase() + title.substring(1);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0,30,0,0);
            textTitle.setLayoutParams(params);

            textTitle.setText(title);
            textTitle.setTextAppearance(this,detailStyle);
            textTitle.setTypeface(textTitle.getTypeface(), Typeface.BOLD);
            detailView.addView(textTitle);
        }
    }

    private void addBodyDetail(String body) {
        if (body != null) {
            TextView textBody = new TextView(getApplicationContext());

            body = body.substring(0, 1).toUpperCase() + body.substring(1);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(40,0,0,0);
            textBody.setLayoutParams(params);

            textBody.setText(body);
            textBody.setTextAppearance(this,detailStyle);
            detailView.addView(textBody);
        }
    }

    private void addDetail(List<String> details, String label) {

        if (details != null && details.size() > 0)
            addTitleDetail(label);

            for (String detail : details) {
                addBodyDetail(detail);
            }
    }

    private void addDetail(String detail, String label) {
        if (detail != null) {
            addTitleDetail(label);
            addBodyDetail(detail);
        }
    }


}
