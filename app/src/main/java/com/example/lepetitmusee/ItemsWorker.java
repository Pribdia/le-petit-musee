package com.example.lepetitmusee;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class ItemsWorker extends AsyncTask<URL, Integer, Void> {
    private Activity activity;

    public ItemsWorker(Activity activity) {
        this.activity = activity;

    }

    @Override
    protected Void doInBackground(URL... urls) {

        HttpURLConnection connectionGetItems;
        InputStream inputStreamGetItems;
        JSONResponseHandlerItems jsonResponseHandlerGetItem = new JSONResponseHandlerItems();

        try {
            URL apiUrlGetItems = WebServiceUrl.buildCatalog();
            connectionGetItems = (HttpURLConnection) apiUrlGetItems.openConnection();
            try {
                inputStreamGetItems = new BufferedInputStream(connectionGetItems.getInputStream());
                jsonResponseHandlerGetItem.readJsonStream(inputStreamGetItems);
            } finally {
                connectionGetItems.disconnect();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void v) {
        super.onPostExecute(v);
        Intent catalogView = new Intent(activity, CatalogActivity.class);
        activity.startActivity(catalogView);
    }
}
