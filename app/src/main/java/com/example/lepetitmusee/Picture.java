package com.example.lepetitmusee;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Picture implements Parcelable {
    private String name;
    private String description;

    public Picture(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.description);
    }

    public static final Creator<Picture> CREATOR = new Creator<Picture>()
    {
        @Override
        public Picture createFromParcel(Parcel source)
        {
            return new Picture(source);
        }

        @Override
        public Picture[] newArray(int size)
        {
            return new Picture[size];
        }
    };

    public Picture(Parcel in) {
        this.name = in.readString();
        this.description = in.readString();
    }
}
