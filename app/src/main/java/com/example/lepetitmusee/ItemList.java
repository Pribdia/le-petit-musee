package com.example.lepetitmusee;

import android.app.Activity;
import android.os.AsyncTask;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ItemList {
    public static List<Item> ITEM_LIST = new ArrayList<Item>();
    private static List<Item> ITEM_LIST_SAVE = new ArrayList<Item>();
    public static List<String> CATEGORY_LIST = new ArrayList<String>();

    // [DEBUT] Tri
    public static void SORT_BY_ALPHABETICAL_ORDER() {
        CLEAR_LIST();

        for (int i = 0; i < ITEM_LIST.size()-1; i++) {
            if (ITEM_LIST.get(i).getName().compareTo(ITEM_LIST.get(i+1).getName()) > 0) {
                SWAP_ITEM(i,i+1);
                i = -1;
            }
        }
    }

    public static void SORT_BY_CHRONOLOGICAL_ORDER() {
        SORT_BY_ALPHABETICAL_ORDER();

        for (int i = 0; i < ITEM_LIST.size()-1; i++) {
            if (ITEM_LIST.get(i).getYear() > ITEM_LIST.get(i+1).getYear()) {
                SWAP_ITEM(i,i+1);
                i = -1;
            }
        }
    }

    public static void SORT_BY_CHRONOLOGICAL_ORDER_WITH_SECTION() {
        SORT_BY_CHRONOLOGICAL_ORDER();

        int currentDecade = -1;
        int itemListSize = ITEM_LIST.size();
        for (int i = 0; i < itemListSize; i++) {
            int itemDecade = GET_DECADE(ITEM_LIST.get(i).getYear());
            if (itemDecade > currentDecade) {
                currentDecade = itemDecade;

                ITEM_LIST.add(i,new ItemSection(currentDecade));
                itemListSize += 1;
            }
        }
    }

    public static void SORT_BY_CATEGORY_WITH_SECTION() {
        SORT_BY_ALPHABETICAL_ORDER();
        SORT_CATEGORY_BY_ALPHABETICAL_ORDER();

        List<Item> tmpItemList = new ArrayList<Item>();

        for (int i = 0; i < CATEGORY_LIST.size(); i++) {
            tmpItemList.add(new ItemSection(CATEGORY_LIST.get(i)));
            tmpItemList.addAll(GET_ITEM_BY_CATEGORY(CATEGORY_LIST.get(i)));
        }
        ITEM_LIST.clear();
        ITEM_LIST = tmpItemList;

    }

    public static List<Item> FILTER_LIST_BY_SEARCH(String stringSearch) {
        CLEAR_LIST();
        List<Item> filteredList = new ArrayList<Item>();
        for (Item item : ITEM_LIST) {
            if (item instanceof ItemSection || item.isResearch(stringSearch)) {
                filteredList.add(item);
            }
        }

        return filteredList;
    }
    // [FIN] Tri

    // [DEBUT] Outils itemList
    public static void CLEAR_LIST() {
        ITEM_LIST.clear();
        ITEM_LIST.addAll(ITEM_LIST_SAVE);
    }

    public static int GET_DECADE(int year) {
        int decade;

        decade = year/10 ;
        decade *= 10 ;

        return decade;
    }

    public static int GET_MIN_DECADE() {
        int minYear = GET_MIN_YEAR();
        int minDecade = GET_DECADE(minYear);

        return minDecade;
    }

    private static void SWAP_ITEM(int firstIndex,int secondIndex) {
        Item tmpItem;
        tmpItem = ITEM_LIST.get(firstIndex);
        ITEM_LIST.set(firstIndex,ITEM_LIST.get(secondIndex));
        ITEM_LIST.set(secondIndex,tmpItem);
    }

    private static void SWAPT_CATEGORY(int firstIndex,int secondIndex) {
        String tmpCategory;
        tmpCategory = CATEGORY_LIST.get(firstIndex);
        CATEGORY_LIST.set(firstIndex,CATEGORY_LIST.get(secondIndex));
        CATEGORY_LIST.set(secondIndex,tmpCategory);
    }

    private static int GET_MIN_YEAR() {
        int minYear = Calendar.getInstance().get(Calendar.YEAR);;
        for (int i = 0; i < ITEM_LIST.size(); i++) {
            if (ITEM_LIST.get(i).getYear() > 0 && ITEM_LIST.get(i).getYear() < minYear) {
                minYear = ITEM_LIST.get(i).getYear();
            }
        }
        return minYear;
    }

    public static void UPDATE_CATEGORY_LIST() {
        CategoryWorker categoryWorker;

        categoryWorker = new CategoryWorker();

        if (categoryWorker.getStatus() == AsyncTask.Status.PENDING) {
            categoryWorker.execute();
        }
    }

    public static List<Item> GET_ITEM_BY_CATEGORY(String category) {
        List<Item> categoryItems = new ArrayList<Item>();
        for (int i = 0; i < ITEM_LIST.size(); i++) {
            if (ITEM_LIST.get(i).isInCategory(category)) {
                categoryItems.add(ITEM_LIST.get(i));
            }
        }
        return categoryItems;
    }



    public static void SORT_CATEGORY_BY_ALPHABETICAL_ORDER() {
        for (int i = 0; i < CATEGORY_LIST.size()-1; i++) {
            if (CATEGORY_LIST.get(i).compareTo(CATEGORY_LIST.get(i+1)) > 0) {
                SWAPT_CATEGORY(i,i+1);
                i = -1;
            }
        }
    }

    // [DEBUT] GETTER et SETTER
    public static void GET_CATALOG(Activity activity) {

        ItemsWorker itemsWorker;

        itemsWorker = new ItemsWorker(activity);

        if (itemsWorker.getStatus() == AsyncTask.Status.PENDING) {
            itemsWorker.execute();
        }


    }

    public static void SET_ITEM_LIST(List<Item> itemList) {
        ITEM_LIST = itemList;
    }

    public static List<Item> GET_ITEM_LIST() {
        return ITEM_LIST;
    }

    public static List<String> GET_CATEGORY() {
        return CATEGORY_LIST;
    }

    public static void SET_CATEGORY(List<String> categoryList) {
        CATEGORY_LIST = categoryList;
    }

    public static List<Item> GET_ITEM_LIST_SAVE() {
        return ITEM_LIST_SAVE;
    }

    public static void SET_ITEM_LIST_SAVE(List<Item> itemListSave) {
        ITEM_LIST_SAVE = itemListSave;
    }

    public static void CLEAR_LIST_SAVE() {
        ITEM_LIST_SAVE.clear();
    }
    // [FIN] GETTER et SETTER

}
