package com.example.lepetitmusee;

public class ItemSection extends Item {

    public ItemSection(int year) {
        String name = "\n\t\t\t -- " + year + " -- ";
        setName(name);
        setSection(true);
    }

    public ItemSection(String category) {
        String name = "\n\t\t\t -- " + category.toUpperCase() + " -- ";
        setName(name);
        setSection(true);
    }

}
