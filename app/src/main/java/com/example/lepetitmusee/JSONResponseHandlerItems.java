package com.example.lepetitmusee;

import android.graphics.Bitmap;
import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class JSONResponseHandlerItems {
    private static final String TAG = JSONResponseHandlerItems.class.getSimpleName();

    private Item itemCurrent;

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readItems(reader);
        } finally {
            reader.close();
        }
    }

    public void readItems(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            itemCurrent = new Item();
            String itemId = reader.nextName();
            if (reader.peek() == JsonToken.BEGIN_OBJECT ) {
                itemCurrent.setId(itemId);
                readArrayItems(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayItems(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("name")) {
                itemCurrent.setName(reader.nextString());
            } else if (name.equals("categories")) {
                reader.beginArray();
                while (reader.hasNext()) {
                    itemCurrent.addCategory(reader.nextString());
                }
                reader.endArray();
            } else if (name.equals("description")) {
                itemCurrent.setDescription(reader.nextString());
            } else if (name.equals("timeFrame")) {
                reader.beginArray();
                while (reader.hasNext()) {
                    itemCurrent.addTimeFrame(reader.nextInt());
                }
                reader.endArray();
            } else if (name.equals("pictures")) {
                reader.beginObject();
                while (reader.hasNext()) {
                    itemCurrent.addPicture(reader.nextName(),reader.nextString());
                }
                reader.endObject();
            } else if (name.equals("year")) {
                itemCurrent.setYear(reader.nextInt());
            } else if (name.equals("brand")) {
                itemCurrent.setBrand(reader.nextString());
            } else if (name.equals("working")) {
                itemCurrent.setWorkig(reader.nextBoolean());
            } else if (name.equals("technicalDetails")) {
                reader.beginArray();
                while (reader.hasNext()) {
                    itemCurrent.addTechnicalDetails(reader.nextString());
                }
                reader.endArray();
            } else {
                reader.skipValue();
            }
        }
        ItemList.GET_ITEM_LIST().add(itemCurrent);
        ItemList.GET_ITEM_LIST_SAVE().add(itemCurrent);
        reader.endObject();
    }
}
