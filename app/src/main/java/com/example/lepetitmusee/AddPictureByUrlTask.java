package com.example.lepetitmusee;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AddPictureByUrlTask extends AsyncTask<String, Void, List<Bitmap>> {
    LinearLayout pictureView;
    Item item;
    Activity activity;

    public AddPictureByUrlTask(Activity activity, LinearLayout pictureView, Item item) {
        this.pictureView = pictureView;
        this.item = item;
        this.activity = activity;
    }

    protected List<Bitmap> doInBackground(String... urls) {
        List<Bitmap> bmpList = new ArrayList<Bitmap>();
        List<Picture> pictureList = item.getPictures();

        for (Picture picture : pictureList) {
            Bitmap bmp = null;
            try {

                URL imageURL = WebServiceUrl.buildPictures(item.getId(),picture.getName());
                System.out.println(imageURL);

                BitmapByURL bitmapByURL = new BitmapByURL(imageURL);
                bmp = bitmapByURL.getImageBitmap();

                if (bmp != null) {
                    bmpList.add(bmp);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        return bmpList;
    }

    protected void onPostExecute(List<Bitmap> result) {
        ImageView imageView = null;
        if (result != null && result.size() > 0) {
            for (Bitmap bitmap : result) {

                imageView = new ImageView(activity);
                imageView.setImageBitmap(bitmap);
                pictureView.setGravity(Gravity.CENTER);
                pictureView.addView(imageView);
            }
        } else {
            TextView textPicture = new TextView(activity);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(100,0,0,0);
            textPicture.setLayoutParams(params);

            textPicture.setText("Aucune image n'est disponible.");
            pictureView.setGravity(Gravity.CENTER);
            pictureView.addView(textPicture);

        }
    }
}
